package main

import (
	"Backend/config"
	"Backend/db"
	"fmt"
	"github.com/gin-gonic/gin"
	"log"
)

func init() {
	err := config.Init()
	if err != nil {
		log.Println("配置初始化失败")
		return
	}
	db.DbInit()
}

func main() {
	gin.SetMode(gin.DebugMode)

	err := GetGinEngine().Run(fmt.Sprintf(":%s", config.Config.Server.Port))
	if err != nil {
		log.Println(err)
		return
	}
}
