package db

import (
	"Backend/config"
	"fmt"
	"sync"
	"time"

	"gorm.io/driver/mysql"
	"gorm.io/gorm"
)

var once sync.Once

type database struct {
	instance    *gorm.DB
	maxIdle     int           // 最大空闲的连接数
	maxOpen     int           // 最大连接数
	maxLifetime time.Duration // 超时时间
}

type Option func(db *database)

var db *database // 全局变量

func WithMaxIdle(maxIdle int) Option {
	return func(d *database) {
		d.maxIdle = maxIdle
	}
}

func WithMaxOpen(maxOpen int) Option {
	return func(d *database) {
		d.maxOpen = maxOpen
	}
}

func WithMaxLifetime(maxLifetime time.Duration) Option {
	return func(d *database) { d.maxLifetime = maxLifetime }
}

func DB(opts ...Option) *gorm.DB {
	once.Do(func() {
		// 先申请空间
		db = new(database)
		for _, f := range opts {
			f(db)
		}

		dsn := fmt.Sprintf("%s:%s@(%s:%s)/%s?charset=utf8mb4&parseTime=True&loc=Local",
			config.Config.Database.User,
			config.Config.Database.Password,
			config.Config.Database.IP,
			config.Config.Database.Port,
			config.Config.Database.DB,
		)
		var err error
		db.instance, err = gorm.Open(mysql.Open(dsn), &gorm.Config{})

		if err != nil {
			panic(err)
		}

		sqlDB, err := db.instance.DB()
		if err != nil {
			panic(err)
		}

		if db.maxLifetime != 0 {
			sqlDB.SetConnMaxLifetime(db.maxLifetime)
		}

		if db.maxIdle != 0 {
			sqlDB.SetMaxIdleConns(db.maxIdle)
		}

		if db.maxOpen != 0 {
			sqlDB.SetMaxOpenConns(db.maxOpen)
		}
	})

	return db.instance
}
