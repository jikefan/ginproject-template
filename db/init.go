package db

import (
	"fmt"
	"time"
)

func DbInit() {
	con := DB(WithMaxIdle(10), WithMaxOpen(100), WithMaxLifetime(time.Minute*30))

	// 自动迁移
	err := con.AutoMigrate()
	if err != nil {
		fmt.Println(err)
		return
	}
}
