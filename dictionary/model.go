package dictionary

type Dictionary struct {
	Key   string `json:"key" gorm:"primaryKey;type:varchar(255)"`
	Value string `json:"value"`
}
