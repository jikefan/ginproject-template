package main

import (
	"Backend/controller/hello"
	"github.com/gin-contrib/cors"
	"github.com/gin-gonic/gin"
)

func GetGinEngine() *gin.Engine {
	route := gin.Default()

	route.Use(cors.New(cors.Config{
		AllowMethods:     []string{"GET", "POST", "PUT", "PATCH", "DELETE", "HEAD"},
		AllowHeaders:     []string{"Origin", "Content-Length", "Content-Type", "Authorization"},
		AllowCredentials: true,
		AllowAllOrigins:  true,
	}))

	api := route.Group("/api")
	{
		api.GET("/ping", hello.GetHello)
	}

	return route
}
