package hello

import (
	"Backend/utils"
	"github.com/gin-gonic/gin"
)

func GetHello(ctx *gin.Context) {
	utils.Success("Hello Gin", ctx)
	return
}
