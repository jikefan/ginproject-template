package config

import (
	ini "github.com/go-ini/ini"
	"log"
	"os"
)

type Database struct {
	IP       string `ini:"ip"`
	Port     string `ini:"port"`
	User     string `ini:"user"`
	Password string `ini:"password"`
	DB       string `ini:"db"`
}

type Camunda struct {
	Host     string `ini:"host"`
	User     string `ini:"user"`
	Password string `ini:"password"`
}

type Server struct {
	Port string `ini:"port"`
}

type SecLogServer struct {
	Port int `ini:"port"`
}

type Neo4jSever struct {
	IP       string `ini:"ip"`
	Username string `ini:"username"`
	Password string `ini:"password"`
}

type LinkageParams struct {
	Username string `ini:"username"`
	Password string `ini:"password"`
	Interval string `ini:"interval"` // 单位分钟，平台可定义token超时时间
	ApiKey   string `ini:"api_key"`
}

type IniConfig struct {
	Database      `ini:"database"`
	Camunda       `ini:"camunda"`
	Server        `ini:"server"`
	SecLogServer  `ini:"sec-log-server"`
	Neo4jSever    `ini:"neo4j-sever"`
	LinkageParams `ini:"linkage-params"`
}

var Config = new(IniConfig)

func Init() error {
	confPath := ""
	if (len(os.Args) == 3) && (os.Args[1] == "-conf") {
		confPath = os.Args[2]
	}
	log.Printf("配置文件路径: %s\n", confPath)
	return InitFromFile(confPath)
}

func InitFromFile(path string) error {
	cfg, err := ini.Load(path)
	if err != nil {
		return err
	}

	return cfg.MapTo(Config)
}
