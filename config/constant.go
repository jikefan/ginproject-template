package config

const LogExample = `<1> 2023-07-11 18:50:24 NSA 192.168.100.9 cc:d3:9d:99:8a:6d 11 E01 7 {"src_ip":"10.0.0.163","dst_ip":"10.0.0.158","src_port":45400,"dst_port":21,"action":"","src_mac":"00:00:e8:7c:dc:b4","dst_mac":"00:01:02:96:23:04","protocol":"tcp"}`

// THREATS_BASE_LINE_KEY 威胁底线， 让攻击链达到这个底线时，需要进行威胁处置
const THREATS_BASE_LINE_KEY = "threats_base_line"
