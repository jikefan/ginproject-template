package utils

import (
	"github.com/gin-gonic/gin"
	"net/http"
	"os"
	"reflect"
	"strconv"
)

// 第一个参数传值，第二个参数传地址
func MapTo(a interface{}, b interface{}) {
	// 使用反射将A的值映射到B
	aValue := reflect.ValueOf(a)
	bValue := reflect.ValueOf(b).Elem() // 获取B的可写副本

	for i := 0; i < aValue.NumField(); i++ {
		aField := aValue.Type().Field(i)
		bField := bValue.FieldByName(aField.Name)

		if bField.IsValid() && bField.Type() == aField.Type {
			bField.Set(aValue.Field(i))
		}
	}
}

func CreateBpmnFile(xmlString string) (*os.File, error) {
	xmlFile, err := os.CreateTemp("", "*.bpmn")
	if err != nil {
		return nil, err
	}

	// defer xmlFile.Close()

	if _, err = xmlFile.WriteString(xmlString); err != nil {
		return nil, err
	}

	return xmlFile, nil
}

// Result 返回的对象
type Result struct {
	Code int         `json:"code"`
	Msg  string      `json:"msg"`
	Data interface{} `json:"data"`
}

type Result0 struct {
	Code int    `json:"code"`
	Msg  string `json:"msg"`
}

const (
	ERROR   = 0
	SUCCESS = 1
)

// Success 封装成功返回的对象
func Success(data interface{}, c *gin.Context) {
	c.JSON(http.StatusOK, Result{
		Code: SUCCESS,
		Msg:  "success",
		Data: data,
	})
}

// Success0 封装成功返回的对象
func Success0(c *gin.Context) {
	c.JSON(http.StatusOK, Result0{
		Code: SUCCESS,
		Msg:  "success",
	})
}

// Fail 封装失败返回的对象
func Fail(msg string, c *gin.Context) {
	c.JSON(http.StatusOK, Result0{
		Code: ERROR,
		Msg:  msg,
	})
}

func KeywordTo32String(keyword string) string {
	var result string
	for _, char := range keyword {
		result += strconv.FormatInt(int64(char), 32)
	}
	return result
}

func MustGetRouterParamUint(key string, c *gin.Context) (uint, error) {
	value := c.Param(key)

	uintVal64, err := strconv.ParseUint(value, 10, 64)

	if err != nil {
		Fail(key+"必须是整型", c)
		return 0, err
	}

	var uintVal uint = uint(uintVal64)
	return uintVal, err
}
